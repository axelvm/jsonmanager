const Xsd2JsonSchema = require('xsd2jsonschema').Xsd2JsonSchema;
var createFile = require('create-file');
var fs = require('fs');



const XML_SCHEMA  = fs.readFileSync('./input/convert/schema.xsd', 'utf8');
 
const xs2js = new Xsd2JsonSchema();
 
const convertedSchemas = xs2js.processAllSchemas({
    schemas: {'hello_world.xsd': XML_SCHEMA}
});
const jsonSchema = convertedSchemas['hello_world.xsd'].getJsonSchema();
let fileContent = JSON.stringify(jsonSchema, null, 2); 

 
createFile('./output/converted.json', fileContent, function (err) {
    // file either already exists or is now created (including non existing directories)
    if (err){
        throw err
    } else {
      //console.log(fileContent);
      console.log("OK");
    }

  });

 
 


 
