const jsoc = require("js-object-compare");
var fs = require('fs');


const count = (obj) => {
    var count=0;
    for(var prop in obj) {
       if (obj.hasOwnProperty(prop)) {
          ++count;
       }
    }
    return count;
 }
 const list = (obj) => {
    let list = [];
    for(var prop in obj) {
       if(obj.hasOwnProperty(prop)){
          list.push(prop);
       }
    }
    return list;
 }

 const compare = (obj1, obj2) => {
    var notIn=[];
    for(var prop in obj2) {
        if (obj1.hasOwnProperty(prop)) {
            
         }else{
            notIn.push(prop);
         }
    }
    return notIn;
 }


//const obj1 = require('./input/first.json');
var monJson1 = JSON.parse(fs.readFileSync('./input/compare/first.json', 'utf8'));
var monJson2 = JSON.parse(fs.readFileSync('./input/compare/second.json', 'utf8'));

console.log(jsoc.sameContent(monJson1, monJson2));
//console.log(jsoc.in_A_ButNotIn_B(monJson2.RIN, monJson1.RIN));
//console.log(jsoc.equalKey(monJson2.RIN, monJson1.RIN).properties);
obj = jsoc.equalKey(monJson2, monJson1);

//RESULT OUTPUT
console.log("nombre de clés first : ", count(monJson1));
console.log("nombre de clés second : ", count(monJson2));
console.log("nombre de clés en commun : ", count(obj));

let response = compare(monJson1, monJson2);
let response2 = compare(monJson2, monJson1);
let responseListObj1 = list(monJson1);



