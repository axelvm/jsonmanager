const jsoc = require("js-object-compare");
var fs = require('fs');


const count = (obj) => {
    var count=0;
    for(var prop in obj) {
       if (obj.hasOwnProperty(prop)) {
          ++count;
       }
    }
    return count;
 }
 const list = (obj) => {
    let list = [];
    for(var prop in obj) {
       if(obj.hasOwnProperty(prop)){
          list.push(prop);
       }
    }
    return list;
 }

 const listDescription = (obj) => {
   let list = [];
  
   for(var prop in obj) {
      if(obj.hasOwnProperty(prop)){
         list.push(obj[prop].description);
      }else{
         list.push("xxx");
      }
   }
   return list;
}

const listType = (obj) => {
   let list = [];
   for(var prop in obj) {
      if(obj.hasOwnProperty(prop)){
         list.push(obj[prop].type);
      }else{
         list.push("xxx");
      }
   }
   return list;
}



//const obj1 = require('./input/first.json');
var monJson1 = JSON.parse(fs.readFileSync('./input/toExcel/first.json', 'utf8'));


//RESULT OUTPUT
console.log("nombre de clés first : ", count(monJson1));

let properties = list(monJson1);
let descriptions = listDescription(monJson1);
let types = listType(monJson1);
console.log("CHAMPS : ");
for(let i in properties){
   console.log(properties[i]);
}
console.log("DESCRIPTIONS :")
for(let i in descriptions){
    console.log(descriptions[i]);
}

console.log("TYPES : ")
for(let i in types){
   console.log(types[i]);
}

