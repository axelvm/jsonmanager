const arrayDifferences = require('array-differences');
const request = require('request');
const readline = require('readline');
var fs = require('fs');
const filename = "input/compareList/test.txt"
const rdoFilename = "input/compareList/rdoFile.txt"

const getAccoutNumbers = (body) =>{
    let accountList = [];
    for(let i=0; i<body.length; i++){
        let accountArray = body[i].data.loyaltyCardAccountList;
        for(let j=0; j<accountArray.length; j++){
            accountList.push(accountArray[j].loyaltyAccountNumber);
        }
    }
    return accountList;
}

const getScrollId = (body) => {

}
//READ FILE
const getAccountNotInRdo = (patch) => {
  let accountNotInRdo = [];
  for(let i=0; i<patch.length; i++){
    if(patch[i][0] === 'inserted'){
        accountNotInRdo.push(patch[i][2])
    }
    //console.log(patch[i][1]);
  }
  return accountNotInRdo;
}

const readAccountList = (filename) =>{
  let List = fs.readFileSync(filename).toString().split("\r\n");
  return List;
}

const readRdoFile = (filename) =>{
  let regex = /['"]+/g
  let ListOfAccount =[];
  let List = fs.readFileSync(filename).toString().split("\r\n");
  for(let i=0; i<List.length; i++){
    let row = List[i].split("|");
    ListOfAccount.push(row[2].replace(regex,""));
  }
  return ListOfAccount;
}



//API Call
const options = {
  url: 'https://api-arf-prive.fr.auchan.com:8482/fr/repositories/loyaltycards/v1?processDate:gte=2019-11-26T03:00&processDate:lte=2019-11-26T08:00&size=10000&scroll=30s',
  headers: {
    'X-Gravitee-Api-Key': 'bdeef010-82d2-49e8-a011-7fe055695750'
  }
};

function callback(error, response, body) {
  if (!error && response.statusCode == 200) {
    const info = JSON.parse(body);
    accountList = getAccoutNumbers(info); 

    return accountList;
  }
}

//let APIResult = request(options, callback);
let expectedList = readAccountList(filename);
//console.log(typeof expectedList);
//console.log(typeof APIResult);
//console.log(APIResult)
let listRdoAccount = readRdoFile(rdoFilename);
console.log(expectedList);


let diff = arrayDifferences(listRdoAccount, expectedList)
let notInRdo = getAccountNotInRdo(diff);
console.log(notInRdo);