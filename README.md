<h1>TUTO V1</h1>
<h2>comparaison</h2>
<p>Pour comparer les propriétés de 2 JSON, mettez la partie properties de votre schéma JSON dans les fichier first et second présent dans le donssier Input</p>
<p>Pour lancer ensuite la comparaison utilisez le script suivant : node compare.js</p>

<h2>Conversion</h2>
<p> Pour convertir votre schéma xsd en fichier JSON, compléter la variable XML_SCHEMA dans le fichier convert.js et lancez le script node convert.js. Votre JSON sera donc présent dans le dossier output</p>

